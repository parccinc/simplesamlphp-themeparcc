<?php

function baseReturnUrl($entityid) {
  //Default production
  //$default_instance = 'https://prc.parcconline.org';

  //Default dev/staging/etc
  $default_instance = 'https://stable.parcc-prc.breaktech.org';

  if ($entityid) {
    $prc_instances = array(
      'https://prc.parcconline.org',
      'http://parcc-prc.dd:8083',
      'https://stable.parcc-prc.breaktech.org',
      'http://int.parcc-prc.breaktech.org',
      'http://parcc-tap.dd:8083',
      'http://bt-qa-tap-elb-324870194.us-east-1.elb.amazonaws.com',
      'https://stable.tap.parcc-ads.breaktech.org',
      'http://parcc-ads.dev',
      'http://uat.parcc-prc.breaktech.org'
    );
    //Base url
    $prc_base = in_array($entityid, $prc_instances) ? $entityid : $default_instance;
  }
  else {
    $prc_base = $default_instance;
  }
  return $prc_base;
}

?>
