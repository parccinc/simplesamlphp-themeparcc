<?php
$this->data['header'] = $this->t('{login:user_pass_header}');

if (strlen($this->data['username']) > 0) {
	$this->data['autofocus'] = 'password';
} else {
	$this->data['autofocus'] = 'username';
}

$file = 'includes/instance_url.php';
$filename = $this->findTemplatePath($file);
include_once($filename);

$this->includeAtTemplateBase('includes/header.php');
$entity_id = isset($this->data['SPMetadata']['entityid']) ? $this->data['SPMetadata']['entityid'] : '';
$prc_base = baseReturnUrl($entity_id);
?>

<h1>Login</h1>
        <form name="loginform" id="loginform" action="?" method="post">
                        <!-- <img alt="logo" src="<?php echo SimpleSAML_Module::getModuleURL('themeparcc/parcclogo.png') ?>" style="float: right" /> -->

                <p>
                        <!-- <label><?php echo $this->t('{login:username}'); ?><br /> -->
                        <label>Email: <span class="form-required" title="This field is required.">*</span></label>
                        <input type="text" name="username" id="username" class="input" <?php if (isset($this->data['username'])) {
                                                echo 'value="' . htmlspecialchars($this->data['username']) . '"';
                                        } ?> size="20" tabindex="10" /></label>
                        <br>
                        <span class='instructions'>Enter your e-mail address.</span>
                </p>
                <p>
                        <!-- <label><?php echo $this->t('{login:password}'); ?><br /> -->
                        <label>Password: <span class="form-required" title="This field is required.">*</span></label>
                        <input type="password" name="password" id="user_pass" class="input" value="" size="20" tabindex="20" /></label>
                        <br>
                        <span class='instructions'>Enter the password that accompanies your e-mail.</span>
                </p>
                <!-- p><label><input name="rememberme" type="checkbox" id="rememberme" value="forever" tabindex="90" /> Remember me</label></p -->
                <p class="submit">
                        <!--
                        <input type="submit" name="wp-submit" id="wp-submit" value="<?php echo $this->t('{login:login_button}'); ?> &raquo;" tabindex="100" />
                        -->
                 <input type="submit" name="wp-submit" id="wp-submit" value="Log in" tabindex="100" />
                 <a class="button parcc-button" href="<?php echo $prc_base; ?>/user/register">Create new account</a>
                 <a class="button parcc-button" href="<?php echo $prc_base; ?>/user/password">Forgot Password?</a>
                </p>



<?php
if ($this->data['errorcode'] !== NULL) {
?>
        <div id="error">
                <img src="/<?php echo $this->data['baseurlpath']; ?>resources/icons/experience/gtk-dialog-error.48x48.png" style="float: right; margin: 15px " />
                <h2><?php echo $this->t('{login:error_header}'); ?></h2>
                <p style="clear: both"><b><?php echo $this->t('{errors:title_' . $this->data['errorcode'] . '}'); ?></b></p>
                <p><?php echo $this->t('{errors:descr_' . $this->data['errorcode'] . '}'); ?></p>
        </div>
<?php
}



if(!empty($this->data['links'])) {
        echo '<ul class="links" style="margin-top: 2em">';
        foreach($this->data['links'] AS $l) {
                echo '<li><a href="' . htmlspecialchars($l['href']) . '">' . htmlspecialchars($this->t($l['text'])) . '</a></li>';
        }
        echo '</ul>';
}



?>


<!--
        <?php if (isset($this->data['error'])) { ?>
                <div id="error">
                <img src="/<?php echo $this->data['baseurlpath']; ?>resources/icons/experience/gtk-dialog-error.48x48.png" style="float: left; margin: 15px " />
                <h2><?php echo $this->t('{error:error_header}'); ?></h2>

                <p style="padding: .2em"><?php echo $this->t($this->data['error']); ?> </p>
                </div>
        <?php } ?>



<?php
if ($this->data['errorcode'] !== NULL) {
?>
        <div id="error">
                <img src="/<?php echo $this->data['baseurlpath']; ?>resources/icons/experience/gtk-dialog-error.48x48.png" style="float: left; margin: 15px " />
                <h2><?php echo $this->t('{login:error_header}'); ?>sdfsdf</h2>
                <p><b><?php echo $this->t('{errors:title_' . $this->data['errorcode'] . '}'); ?></b></p>
                <p><?php echo $this->t('{errors:descr_' . $this->data['errorcode'] . '}'); ?></p>
        </div>
<?php
}
?>
                -->

<?php
foreach ($this->data['stateparams'] as $name => $value) {
        echo('<input type="hidden" name="' . htmlspecialchars($name) . '" value="' . htmlspecialchars($value) . '" />');
}
?>

        </form>





</div>
</div>

<?php


        $includeLanguageBar = FALSE;
        if (!empty($_POST))
                $includeLanguageBar = FALSE;
        if (isset($this->data['hideLanguageBar']) && $this->data['hideLanguageBar'] === TRUE)
                $includeLanguageBar = FALSE;

        if ($includeLanguageBar) {


                echo '<div id="languagebar">';

                // echo '<form action="' . SimpleSAML_Utilities::selfURL() . '" method="get">';
                // echo '<select name="language">';
                // echo '</select>';
                // echo '</form>';



                $languages = $this->getLanguageList();
                $langnames = array(
                        'no' => 'Bokmål',
                        'nn' => 'Nynorsk',
                        'se' => 'Sámegiella',
                        'sam' => 'Åarjelh-saemien giele',
                        'da' => 'Dansk',
                        'en' => 'English',
                        'de' => 'Deutsch',
                        'sv' => 'Svenska',
                        'fi' => 'Suomeksi',
                        'es' => 'Español',
                        'eu' => 'Euskara',
                        'fr' => 'Français',
                        'nl' => 'Nederlands',
                        'lb' => 'Luxembourgish',
                        'cs' => 'Czech',
                        'sl' => 'Slovenščina', // Slovensk
                        'hr' => 'Hrvatski', // Croatian
                        'hu' => 'Magyar', // Hungarian
                        'pl' => 'Język polski', // Polish
                        'pt' => 'Português', // Portuguese
                        'pt-br' => 'Português brasileiro', // Portuguese
                        'tr' => 'Türkçe',
                );

                $textarray = array();
                foreach ($languages AS $lang => $current) {
                        if ($current) {
                                $textarray[] = $langnames[$lang];
                        } else {
                                $textarray[] = '<a href="' . htmlspecialchars(
                                                SimpleSAML_Utilities::addURLparameter(
                                                        SimpleSAML_Utilities::selfURL(), array('language' => $lang)
                                                )
                                ) . '">' . $langnames[$lang] . '</a>';
                        }
                }
                echo join(' | ', $textarray);
                echo '</div>';
        }
?>

 
<?php
  $this->includeAtTemplateBase('includes/footer.php');
?>
